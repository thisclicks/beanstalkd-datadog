# Read Me #

* Put beanstalkd.py in /etc/dd-agent/checks.d/
* Put beanstalkd.yaml in /etc/dd-agent/conf.d/
* Restart dd-agent

## Dependencies ##

* [Beanstalkc](https://github.com/earl/beanstalkc)
* [PyYAML](http://pyyaml.org/)

## Datadog Docs ##
[Agent Checks](http://docs.datadoghq.com/guides/agent_checks/)